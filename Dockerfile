FROM golang:1.15-alpine as builder

WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go build -o /server ./main.go

FROM node:current-alpine as frontend-builder

RUN apk add git yarn

WORKDIR /src/app
COPY ./app .

RUN yarn install && yarn build

FROM alpine:latest

COPY --from=builder /server /server

COPY --from=frontend-builder /src/app/build /app

CMD /server
