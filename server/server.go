package server

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/coreos/go-oidc"
	"github.com/gin-gonic/gin"
	"github.com/heptiolabs/healthcheck"
	"github.com/joho/godotenv"
	"github.com/minio/minio-go/v7"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
	ginoauth2 "github.com/zalando/gin-oauth2"
	"gitlab.com/toastys-homelab/vaadinar/server/documents"
	"gitlab.com/toastys-homelab/vaadinar/server/gitlab"
	"gitlab.com/toastys-homelab/vaadinar/server/user"
	"golang.org/x/oauth2"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const PostgresEnvFile = "postgres.env"
const HealtcheckPort = "8086"

func getStaticRoot() string {
	if path, ok := os.LookupEnv("APP_PATH"); ok {
		return path
	}
	return "/app"
}

func buildDSN() (string, error) {
	if gitlab.IsProductionEnvironment() || gitlab.IsReviewEnvironment() {
		return gitlab.GetAutoDevOpsPostgresDatabaseDSN(), nil
	}

	if _, err := os.Stat(PostgresEnvFile); err == nil {
		config, err := godotenv.Read(PostgresEnvFile)
		if err != nil {
			return "", fmt.Errorf("could not read postgres env file enven though it exists: %w", err)
		}
		return fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Europe/Zurich",
			"localhost", "postgres", config["POSTGRES_PASSWORD"], config["POSTGRES_DB"], "5432"), nil
	}

	dbUrl := os.Getenv("DATABASE_URL")
	if dbUrl != "" {
		return dbUrl, nil
	}

	return "", fmt.Errorf("if you see this error, the application is in devlopment mode and has no %s file present in the working directory. Gitlab the gitlab environment is %s", PostgresEnvFile, gitlab.Environment())
}

type Server struct {
	db              *gorm.DB
	healthCheck     healthcheck.Handler
	router          *gin.Engine
	listen          string
	oauthConfig     oauth2.Config
	oidcProvider    *oidc.Provider
	oidcVerifier    *oidc.IDTokenVerifier
	minioClient     *minio.Client
	documentsBucket string
	kafkaConn       *kafka.Conn
}

func (srv *Server) openDBConnection(dsn string) error {
	var err error
	logrus.Infof("Opening Database connection")
	srv.db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return err
	}

	if sqlDB, err := srv.db.DB(); err == nil {
		srv.healthCheck.AddReadinessCheck("database", healthcheck.DatabasePingCheck(sqlDB, 3*time.Second))
	} else {
		panic(fmt.Errorf("could not add readinesscheck for database not a database connection: %w", err))
	}

	return nil
}

func (srv *Server) buildRouters() error {
	srv.router = gin.Default()
	srv.router.Use(ginoauth2.RequestLogger([]string{"uid"}, "data"))
	srv.router.GET("/health", gin.WrapH(srv.healthCheck))

	staticRoot := getStaticRoot()
	srv.router.Static("/static", staticRoot+"/static")
	files, err := ioutil.ReadDir(staticRoot)
	if err != nil {
		return err
	}

	srv.router.StaticFile("/", staticRoot+"/index.html")
	for _, f := range files {
		srv.router.StaticFile("/"+f.Name(), staticRoot+"/"+f.Name())
	}

	apiRouterGroup := srv.router.Group("/api/v1")
	//apiRouterGroup.Use(ginoauth2.Auth(NoCheck, srv.oidcProvider.Endpoint()))
	user.NewUserRouterGroup(apiRouterGroup, srv.db)
	documents.NewDocumentsRouterGroup(apiRouterGroup, srv.minioClient, srv.documentsBucket, srv.db, srv.kafkaConn)

	return nil
}

func (srv *Server) MigrateDB() error {

	return nil
}

func (srv *Server) Run() error {
	return srv.router.Run(srv.listen)
}

func New(listen string) (*Server, error) {
	dsn, err := buildDSN()
	if err != nil {
		return nil, err
	}

	if !strings.Contains(listen, ":5000") {
		listen += ":5000"
	}

	srv := &Server{
		listen: listen,
	}
	srv.healthCheck = healthcheck.NewHandler()

	srv.documentsBucket = os.Getenv("S3_DOCUMENT_BUCKET")

	//logrus.Infof("Loading Auth settings")
	//keyCloakConfig, err := LoadKeyCloakSettings()
	//if err != nil {
	//	return nil, err
	//}
	//
	//logrus.Infof("Starting OpenID Client")
	//if err := srv.keyCloakSetUpOauth(keyCloakConfig); err != nil {
	//	return nil, err
	//}

	logrus.Infof("Starting S3 Client and creating buckets")
	srv.minioClient, err = openS3Connection(srv.documentsBucket)
	if err != nil {
		return nil, err
	}

	logrus.Infof("Starting connection to Kafka")
	kafkaHost := "localhost:9092"
	if envVal, ok := os.LookupEnv("KAFKA_ADVERTISED_HOST_NAME"); ok {
		kafkaHost = envVal + ":" + os.Getenv("KAFKA_ADVERTISED_PORT")
	}
	ctx, _ := context.WithTimeout(context.Background(), 20*time.Second)
	srv.kafkaConn, err = kafka.DialLeader(ctx, "tcp", kafkaHost, "fileInput", 0)
	if err != nil {
		logrus.Fatal("failed to dial leader:", err)
		return nil, err
	}

	logrus.Infof("Connecting to Database")
	if err := srv.openDBConnection(dsn); err != nil {
		return nil, err
	}

	logrus.Infof("Starting REST API")
	if err := srv.buildRouters(); err != nil {
		return nil, err
	}

	logrus.Infof("Starting Database migration tasks")
	if err = srv.MigrateDB(); err != nil {
		return nil, err
	}
	logrus.Infof("Finished DB migration tasks")

	return srv, nil
}
