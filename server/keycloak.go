package server

import (
	"context"
	"encoding/json"
	"net/url"
	"os"
	"path"

	oidc "github.com/coreos/go-oidc"
	"github.com/gin-gonic/gin"
	ginoauth2 "github.com/zalando/gin-oauth2"
	"gitlab.com/toastys-homelab/vaadinar/server/gitlab"
	"golang.org/x/oauth2"
)

type KeyCloakConfig struct {
	Realm               string `json:"realm"`
	AuthServerURL       string `json:"auth-server-url"`
	Resource            string `json:"resource"`
	VerifyTokenAudience bool   `json:"verify-token-audience"`
	Credentials         struct {
		Secret string `json:"secret"`
	} `json:"credentials"`
	ConfidentialPort int `json:"confidential-port"`
}

func (srv *Server) keyCloakSetUpOauth(config KeyCloakConfig) error {
	ctx := context.Background()
	providerURL, err := url.Parse(config.AuthServerURL)
	if err != nil {
		return err
	}
	providerURL.Path = path.Join(providerURL.Path, "realms", config.Realm)
	provider, err := oidc.NewProvider(ctx, providerURL.String())
	if err != nil {
		return err
	}
	srv.oauthConfig = oauth2.Config{
		ClientID:     config.Resource,
		ClientSecret: config.Credentials.Secret,
		RedirectURL:  gitlab.EnvironmentURL(),
		// Discovery returns the OAuth2 endpoints.
		Endpoint: provider.Endpoint(),
		// "openid" is a required scope for OpenID Connect flows.
		Scopes: []string{oidc.ScopeOpenID, "profile", "email"},
	}
	srv.oidcProvider = provider

	oidcConfig := &oidc.Config{
		ClientID: config.Resource,
	}
	srv.oidcVerifier = provider.Verifier(oidcConfig)

	return nil
}

func NoCheck(tc *ginoauth2.TokenContainer, ctx *gin.Context) bool {
	return true
}

func LoadKeyCloakSettings() (KeyCloakConfig, error) {
	conf := KeyCloakConfig{}
	fileName := os.Getenv("KEYCLOAK_FILE")
	if fileName == "" {
		fileName = "./keycloak.json"
	}

	f, err := os.Open(fileName)
	if err != nil {
		return conf, err
	}
	defer f.Close()

	if err := json.NewDecoder(f).Decode(&conf); err != nil {
		return conf, err
	}

	return conf, nil
}
