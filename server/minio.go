package server

import (
	"context"
	"os"

	"github.com/joho/godotenv"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

const MinioEnvFile = "minio.env"

func openS3Connection(documentsBucketName string) (*minio.Client, error) {
	var endpoint, accessKeyID, secretAccessKey string
	useSSL := true
	if _, err := os.Stat(MinioEnvFile); err == nil {
		config, err := godotenv.Read(MinioEnvFile)
		if err != nil {
			return nil, err
		}
		endpoint = config["MINIO_ENDPOINT"]
		accessKeyID = config["MINIO_ACCESS_KEY"]
		secretAccessKey = config["MINIO_SECRET_KEY"]
		if documentsBucketName == "" {
			documentsBucketName = config["DOCUMENTS_BUCKET"]
		}
		if _, ok := config["S3_NO_SSL"]; ok {
			useSSL = false
		}
	} else {
		endpoint = os.Getenv("S3_ENDPOINT")
		accessKeyID = os.Getenv("S3_ACCESS_KEY_ID")
		secretAccessKey = os.Getenv("S3_SECRET_ACCESS_KEY")
	}

	ctx := context.Background()

	if _, ok := os.LookupEnv("S3_NO_SSL"); useSSL && ok {
		useSSL = false
	}

	minioClient, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
	})
	if err != nil {
		return nil, err
	}

	if exists, err := minioClient.BucketExists(ctx, documentsBucketName); err == nil {
		if !exists {
			if err := minioClient.MakeBucket(ctx, documentsBucketName, minio.MakeBucketOptions{
				Region: os.Getenv("S3_REGION"),
			}); err != nil {
				return nil, err
			}
		}
	} else {
		return nil, err
	}

	return minioClient, nil
}
