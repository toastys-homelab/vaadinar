package gitlab

import (
	"fmt"
	"os"
	"strings"
)

func Environment() string {
	return os.Getenv("GITLAB_ENVIRONMENT_NAME")
}

func IsProductionEnvironment() bool {
	return Environment() == "production"
}

func IsReviewEnvironment() bool {
	return strings.HasPrefix(Environment(), "review/")
}

func EnvironmentURL() string {
	return os.Getenv("GITLAB_ENVIRONMENT_URL")
}

func GetAutoDevOpsPostgresDatabaseDSN() string {
	dbUrl := os.Getenv("DATABASE_URL")
	if dbUrl != "" {
		return dbUrl
	}

	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		os.Getenv("DB_HOST"), os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASSWORD"), os.Getenv("POSTGRES_DB"), os.Getenv("POSTGRES_PORT"))
}
