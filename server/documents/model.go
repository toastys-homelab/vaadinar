package documents

import "github.com/gofrs/uuid"

type Document struct {
	Id uuid.UUID `json:"id"`
}

type DocumentRecord struct {
	Id    uuid.UUID `json:"id"`
	S3Key string    `json:"s3key"`
}
