package documents

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
	"github.com/minio/minio-go/v7"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

func NewDocumentsRouterGroup(baseGroup *gin.RouterGroup, minioClient *minio.Client, documentsBucketName string, database *gorm.DB, conn *kafka.Conn) *gin.RouterGroup {
	documentGroup := baseGroup.Group("/documents")
	documentGroup.POST("/upload", uploadDocumentGroup(minioClient, documentsBucketName, conn))

	return documentGroup
}

func uploadDocumentGroup(minioClient *minio.Client, bucket string, conn *kafka.Conn) gin.HandlerFunc {
	type Params struct {
		DocumentTypeId uuid.UUID `json:"type"`
		UserID         uuid.UUID `json:"user"`
	}

	return func(ctx *gin.Context) {
		params := Params{}
		//TODO Get user that is logged in from context
		form, err := ctx.MultipartForm()
		if err != nil {
			logrus.Error(err)
			ctx.AbortWithStatus(http.StatusMethodNotAllowed)
			return
		}

		for _, value := range form.Value {
			if err := json.Unmarshal([]byte(value[0]), &params); err != nil {
				logrus.Error(err)
				ctx.AbortWithStatus(http.StatusInternalServerError)
				return
			}
		}

		for _, files := range form.File {
			for _, file := range files {
				rd, err := file.Open()
				if err != nil {
					logrus.Error(err)
					ctx.AbortWithStatus(http.StatusInternalServerError)
					return
				}
				uploadedFullPath := fmt.Sprintf("%s/uploads/%s", params.UserID, file.Filename)

				if _, err := minioClient.PutObject(ctx, bucket, uploadedFullPath, rd, file.Size, minio.PutObjectOptions{
					ContentType: "application/pdf",
				}); err != nil {
					logrus.Error(err)
					ctx.AbortWithStatus(http.StatusInternalServerError)
					return
				}

				object, err := minioClient.GetObject(ctx, bucket, uploadedFullPath, minio.GetObjectOptions{})
				if err != nil {
					logrus.Error(err)
					ctx.AbortWithStatus(http.StatusInternalServerError)
					return
				}

				buff := &bytes.Buffer{}

				if _, err := io.Copy(buff, object); err != nil {
					logrus.Error(err)
					ctx.AbortWithStatus(http.StatusInternalServerError)
					return
				}

				if _, err := conn.WriteMessages(kafka.Message{
					Key:   []byte("file-input"),
					Value: buff.Bytes(),
					Headers: []kafka.Header{
						{Key: "document_type", Value: params.DocumentTypeId.Bytes()},
						{Key: "user", Value: params.DocumentTypeId.Bytes()},
						{Key: "filename", Value: []byte(file.Filename)},
					},
				}); err != nil {
					logrus.Error(err)
					ctx.AbortWithStatus(http.StatusInternalServerError)
					return
				}
			}
		}
	}
}
