package user

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func NewUserRouterGroup(baseGroup *gin.RouterGroup, database *gorm.DB) *gin.RouterGroup {
	userGroup := baseGroup.Group("/user")
	userGroup.GET("/by-token", getByTokenHandler())

	return userGroup
}

func getByTokenHandler() gin.HandlerFunc {
	type Params struct {
	}

	return func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{"nothing": "nope not yet done"})
	}
}
