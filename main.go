package main

import "gitlab.com/toastys-homelab/vaadinar/server"

func main() {
	srv, err := server.New("")
	if err != nil {
		panic(err)
	}

	if err := srv.Run(); err != nil {
		panic(err)
	}
}
