// +heroku goVersion go1.14
// +heroku install ./main.go

module gitlab.com/toastys-homelab/vaadinar

go 1.14

require (
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/heptiolabs/healthcheck v0.0.0-20180807145615-6ff867650f40
	github.com/joho/godotenv v1.3.0
	github.com/minio/minio-go/v7 v7.0.5
	github.com/pquerna/cachecontrol v0.0.0-20200921180117-858c6e7e6b7e // indirect
	github.com/prometheus/client_golang v1.7.1 // indirect
	github.com/segmentio/kafka-go v0.4.6
	github.com/sirupsen/logrus v1.4.2
	github.com/zalando/gin-oauth2 v1.5.2
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
	gorm.io/driver/postgres v1.0.2
	gorm.io/gorm v1.20.2
)
