import camelot
import pprint
from PyPDF2 import PdfFileReader
from kafka import KafkaConsumer
import os

pp = pprint.PrettyPrinter(indent=4)


# consumer = KafkaConsumer('fileInput', bootstrap_servers="localhost:9092")
# print("Starting consumer")
# for msg in consumer:
#     filename = msg.headers[2][1].decode("utf-8")
#     with open(filename, "wb+") as f:
#         f.write(msg.value)
#
#     tables = camelot.read_pdf(filename, flavor='stream')
#     page = 1
#     for table in tables:
#         print(table.parsing_report)
#         table.to_json("page{page}.json".format(page=page))
#         page = page + 1
#
#     os.remove(filename)
def pageStringer(numberList):
    for num in numberList:
        yield str(num)


filename = "/export/home/toast/Documents/Bank/REP_P_CH2009000000870499223_1108198533_0_2020100112472046.pdf"
f = open(filename, "rb")
pdf = PdfFileReader(f)
pages = ",".join(pageStringer(range(1, pdf.getNumPages()+1)))

tables = camelot.read_pdf(filename, flavor='stream', pages=pages)
for table in tables:
    print(table.parsing_report)
    # table.to_csv("page{page}.csv".format(page=page))
    frame = table.df
    statements = []
    statement = {}
    start = "false"
    for (idx, row) in frame.iterrows():
        if start != "true":
            if row.loc[0] == "Datum":
                start = "true"
            continue

        if row.loc[2] != "":
            if bool(statement):
                statements.append(statement)

            statement = {
                "date": row.loc[4],
                "text": row.loc[1],
                "saldo": row.loc[5],
                "incoming": row.loc[2]
            }
        elif row.loc[3] != "":
            if bool(statement):
                statements.append(statement)

            statement = {
                "date": row.loc[4],
                "text": row.loc[1],
                "saldo": row.loc[5],
                "deduction": row.loc[3]
            }
        elif row.loc[3] == "Kontostand":
            statements.append({
                "text": row.loc[3],
                "saldo": row.loc[5]
            })
        else:
            if "text" in statement:
                statement["text"] += "\n" + row.loc[1]

    statements.append(statement)
    pp.pprint(statements)
print(tables[5].df)
