from PyPDF2 import PdfFileReader
import cv2
from camelot.utils import get_page_layout


def get_pages(filename, pages="1", password=""):
    """Converts pages string to list of ints.
    Parameters
    ----------
    filename : str
        Path to PDF file.
    pages : str, optional (default: '1')
        Comma-separated page numbers.
        Example: 1,3,4 or 1,4-end.
    password: str , optional (default: '')
        Password to decrypt the PDF if
        it is encrypted
    Returns
    -------
    number_of_pages : int
        Total pages.
    page_list : list
        List of int page numbers.
    """
    page_numbers = []
    inputstream = open(filename, "rb")
    infile = PdfFileReader(inputstream, strict=False)
    number_of_pages = infile.getNumPages()
    if pages == "1":
        page_numbers.append({"start": 1, "end": 1})
    else:
        if infile.isEncrypted:
            infile.decrypt(password)
        if pages == "all":
            page_numbers.append({"start": 1, "end": infile.getNumPages()})
        else:
            for r in pages.split(","):
                if "-" in r:
                    a, b = r.split("-")
                    if b == "end":
                        b = infile.getNumPages()
                    page_numbers.append({"start": int(a), "end": int(b)})
                else:
                    page_numbers.append({"start": int(r), "end": int(r)})
    inputstream.close()
    page_list = []
    for p in page_numbers:
        page_list.extend(range(p["start"], p["end"] + 1))
    return sorted(set(page_list)), number_of_pages


def get_file_dimensions(filepath):
    layout, dimensions = get_page_layout(filepath)
    return list(dimensions)


def get_image_dimensions(imagepath):
    image = cv2.imread(imagepath)
    return [image.shape[1], image.shape[0]]
