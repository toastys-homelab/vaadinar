import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Login from "./components/Login";
import PrivateRoute from "./components/PrivateRoute";
import Register from "./components/Register";
import Dashboard from "./components/Dashboard";
function App() {

  return (
    <Router>
      <Switch>
        <PrivateRoute exact path="/" >
          <Dashboard />
        </PrivateRoute>
        <Route path="/login">
          <Login/>
        </Route>
        <Route path="/register">
          <Register/>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
