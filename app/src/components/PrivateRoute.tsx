import React, {ComponentProps} from "react";
import {Route, Redirect} from "react-router-dom";
import {useSelector} from "react-redux";
import {RootState} from "../store";

export default function PrivateRoute({ children, ...rest }: ComponentProps<any>) {
    const idToken = useSelector((state: RootState) => state.system.accessToken)

    return (
        <Route
            {...rest}
            render={({ location }) =>
                idToken !== "" ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: { from: location }
                        }}
                    />
                )
            }
        />
    );
}