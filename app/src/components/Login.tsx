import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {TextField, Checkbox} from 'formik-material-ui';
import {
    Container,
    Grid,
    Button,
    FormControlLabel,
    Typography,
    Avatar,
    CssBaseline,
    LinearProgress
} from "@material-ui/core";
import {Link, useHistory} from 'react-router-dom';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {Field, Form, Formik} from "formik";
import * as yup from 'yup';
import {useDispatch} from "react-redux";
import {updateLoginTokens} from "../store/system/actions";

const validatorSchema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().required(),
    remember: yup.boolean().optional(),
})

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Login() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <Formik initialValues={{
                    email: "",
                    password: "",
                    remember: true,
                }} onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                        setSubmitting(false);
                        dispatch(updateLoginTokens("testing", "testing", "testing"))
                        history.push("/")
                    }, 500);
                }} validationSchema={validatorSchema} >
                    {({ values, submitForm, isSubmitting }) => (
                        <Form className={classes.form}>
                            <Field
                                component={TextField}
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                            />
                            <Field
                                component={TextField}
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                            />
                            <FormControlLabel
                                control={<Field type="checkbox" name="remember" component={Checkbox} color="primary" />}
                                label="Remember me"
                            />
                            {isSubmitting && <LinearProgress />}
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                disabled={isSubmitting}
                            >
                                Sign In
                            </Button>
                            <Grid container>
                                <Grid item xs>
                                    <Link to="#">
                                        Forgot password?
                                    </Link>
                                </Grid>
                                <Grid item>
                                    <Link to="/register">
                                        {"Don't have an account? Sign Up"}
                                    </Link>
                                </Grid>
                            </Grid>
                        </Form>
                    )}
                </Formik>
            </div>
        </Container>
    );
}