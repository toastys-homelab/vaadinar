import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from '../Title';
import {v4 as uuidv4} from 'uuid';
import moment from "moment";

const TESTIBAN= ""
const TESTFILENAME= ""

function createFileEntry(start_date: moment.Moment, end_date: moment.Moment, iban: string, filename: string): File {
    return {
        id: uuidv4(),
        start_date,
        end_date,
        account: {
            iban
        },
        filename
    }
}

interface File {
    id: string,
    start_date: moment.Moment,
    end_date: moment.Moment,
    account: {
        iban: string
    }
    filename: string
}

const rows = [
    createFileEntry(moment("2020-08-31T00:00:00"), moment("2020-10-01T00:00:00"), TESTIBAN, TESTFILENAME)
];

export default function Files() {
    return (
        <React.Fragment>
            <Title>Account Statements</Title>
            <Table size="small">
                <TableHead>
                    <TableRow>
                        <TableCell>Dateiname</TableCell>
                        <TableCell>Account</TableCell>
                        <TableCell>Zeitspanne</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.id}>
                            <TableCell>{row.filename}</TableCell>
                            <TableCell>{row.account.iban}</TableCell>
                            <TableCell>{row.start_date.format("DD.MM.YYYY")} - {row.end_date.format("DD.MM.YYYY")}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </React.Fragment>
    );
}
