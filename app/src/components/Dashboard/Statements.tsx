import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from '../Title';
import {v4 as uuidv4} from 'uuid';
import moment from "moment";

function createStatement(date: moment.Moment, text: string, amount_withdrawn: string, amount_deposited: string): AccountStatement {
    return {
        id: uuidv4(),
        date,
        details: {
            text,
        },
        amount_withdrawn,
        amount_deposited
    }
}

interface AccountStatement {
    id: string,
    date: moment.Moment,
    details: {
        text: string,
        [info: string]: string
    }
    amount_withdrawn: string
    amount_deposited: string
}

const rows = [
    createStatement(moment("2020-08-31T00:00:00"), "KAUF/DIENSTLEISTUNG\n" +
        "VOM 31.08.2020\n" +
        "KARTEN NR. XXXXXXXX\n" +
        "VERKàUFER\n" +
        "ORT\n", "6.60", ""),
    createStatement(moment("2020-09-01T00:00:00"), "TRANSFER\n" +
        "99-9999999\n" +
        "CH9999999999999999999\n" +
        "RECIPIENT\n" +
        "RECIPIENT LINE2\n" +
        "SENDER REFERENZ:\n" +
        "ANMERKUNG\n", "2800.00", ""),
    createStatement(moment("2020-09-01T00:00:00"), "BARGELDBEZUG VOM 01.09.2020\n" +
        "KARTEN NR. XXXX9999\n" +
        "EXACT PLACE\n" +
        "CITY\n", "100.00", "")
];

export default function Statements() {
    return (
        <React.Fragment>
            <Title>Account Statements</Title>
            <Table size="small">
                <TableHead>
                    <TableRow>
                        <TableCell>Datum</TableCell>
                        <TableCell>Text</TableCell>
                        <TableCell>Gutschrift</TableCell>
                        <TableCell>Lastschrift</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.id}>
                            <TableCell>{row.date.format("DD.MM.YYYY")}</TableCell>
                            <TableCell>{row.details.text.split("\n").map((line) => (<div>{line}</div>))}</TableCell>
                            <TableCell>{row.amount_deposited}</TableCell>
                            <TableCell>{row.amount_withdrawn}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </React.Fragment>
    );
}
