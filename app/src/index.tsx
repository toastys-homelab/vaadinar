import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';
import {getQueries, makeStore} from "./store";
import { Provider as ReduxQueryProvider } from 'redux-query-react';
import { createServer } from "miragejs";
import {v4 as uuid4} from 'uuid';

const store = makeStore()

createServer({
    namespace: "api/v1",
    routes() {
        this.get("/user/by-token", () => ({
            uuid: uuid4(),
            email_verified: true,
            email: "hi@example.com",
            username: "mockery",
            accessToken: "testing"
        }))
    },
})

ReactDOM.render(
  <Provider store={store}>
      <ReduxQueryProvider queriesSelector={getQueries}>
        <App />
      </ReduxQueryProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
