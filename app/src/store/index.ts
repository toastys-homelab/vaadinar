import {applyMiddleware, combineReducers, compose, createStore} from 'redux'
import {systemReducer} from "./system/reducers";
import { entitiesReducer, queriesReducer, queryMiddleware } from 'redux-query';
import superagentInterface from 'redux-query-interface-superagent';

export const getQueries = (state: any) => state.queries;
export const getEntities = (state: any) => state.entities;

export const rootReducer = combineReducers({
    system: systemReducer,
    entities: entitiesReducer,
    queries: queriesReducer,
})

export type RootState = ReturnType<typeof rootReducer>

export function makeStore() {
    return createStore(
        rootReducer,
        compose(
            applyMiddleware(queryMiddleware(superagentInterface, getQueries, getEntities)),
            // @ts-ignore
            window.devToolsExtension ? window.devToolsExtension() : f => f
        )
    )
}
