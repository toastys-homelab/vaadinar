import {SystemActionTypes, SystemState, UPDATE_LOGIN_TOKENS} from "./types";


const initialState: SystemState = {
    accessToken: "testing",
    idToken: "testing",
    refreshToken: "testing",
}

export function systemReducer(
    state = initialState,
    action: SystemActionTypes
): SystemState {
    switch (action.type) {
        case UPDATE_LOGIN_TOKENS:
            return {
                ...state,
                ...action.payload,
            }
        default:
            return state
    }
}