import {QueryConfig} from "redux-query";
import {SystemState} from "./types";

export function getUserByTokenQuery(token: string): QueryConfig {
    return {
        url: '/api/v1/user/by-token',
        options: {
            headers: {
                Authorization: `Bearer ${token}`
            },
            credentials: "include",
        },
        transform: body => ({user: body}),
        update: {
            user: (oldValue: SystemState, newValue: SystemState) => newValue
        }
    }
}