export interface SystemState {
    accessToken: string
    idToken: string
    refreshToken: string
}

export const UPDATE_LOGIN_TOKENS = 'system/tokens/update'

interface UpdateLoginTokens {
    type: typeof UPDATE_LOGIN_TOKENS
    payload: {
        accessToken: string
        idToken: string
        refreshToken: string
    }
}

export type SystemActionTypes = UpdateLoginTokens