import {SystemActionTypes, UPDATE_LOGIN_TOKENS} from "./types";

export function updateLoginTokens(accessToken: string, idToken: string, refreshToken: string): SystemActionTypes {
    return {
        type: UPDATE_LOGIN_TOKENS,
        payload: {
            accessToken,
            idToken,
            refreshToken,
        }
    }
}