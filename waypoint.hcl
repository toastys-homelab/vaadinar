# The name of your project. A project typically maps 1:1 to a VCS repository.
# This name must be unique for your Waypoint server. If you're running in
# local mode, this must be unique to your machine.
project = "vaadinar"

# An application to deploy.
app "web" {
    # Build specifies how an application should be deployed. In this case,
    # we'll build using a Dockerfile and keeping it in a local registry.
    build {
        use "docker" {}
        
        # Uncomment below to use a remote docker registry to push your built images.
        #
        registry {
          use "docker" {
            #encoded_auth = filebase64("/home/toast/workspace/personal/harborAuth.json")
            #image = "harbor.wegmueller-it.industries/toastys-homelab/vaadinar"

            encoded_auth = filebase64("/home/toast/workspace/personal/dockerAuth.json")
            image = "registry.gitlab.com/toastys-homelab/vaadinar"
            tag   = "latest"
          }
        }

    }

    # Deploy to Docker
    deploy {
        use "kubernetes" {
          probe_path = "/"
          image_secret = "regcred"
          service_port = "5000"
          static_environment = {
            DATABASE_URL = "host=vaadinar-postgresql user=postgres password=vaadinar dbname=vaadinar port=5432 sslmode=disable TimeZone=Europe/Zurich"
            S3_ENDPOINT = "vaadinar-minio:9000"
            S3_ACCESS_KEY_ID = "AKIAIOSFODNN7EXAMPLE"
            S3_SECRET_ACCESS_KEY = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
            S3_NO_SSL = "true"
            S3_DOCUMENT_BUCKET = "documents"
            KAFKA_ADVERTISED_HOST_NAME = "vaadinar-kafka"
            KAFKA_ADVERTISED_PORT = "9092"
          }
        }
    }
}
